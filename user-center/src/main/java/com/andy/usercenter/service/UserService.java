package com.andy.usercenter.service;

import com.andy.usercenter.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分享 服务类
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
public interface UserService extends IService<User> {

}
