package com.andy.usercenter.controller;


import com.andy.usercenter.entity.User;
import com.andy.usercenter.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;


/**
 * <p>
 * 分享 前端控制器
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserMapper userMapper;
    @GetMapping("/{id}")
    public User findById(@PathVariable Integer id) {
        log.info("我被请求了...");
        return this.userMapper.selectById(id);
    }

    @GetMapping("getId")
    public User getId(Integer id){
        return this.userMapper.selectById(id);
    }

    @GetMapping("/test")
    public User testInsert(){
        User user = new User();
        user.setAvatarUrl("xxxxx");
        user.setBonus(100);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        this.userMapper.insert(user);
        return user;
    }

}
