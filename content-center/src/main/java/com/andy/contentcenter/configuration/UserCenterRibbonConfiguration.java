package com.andy.contentcenter.configuration;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Configuration;
import ribbonconfiguration.RibbonConfiguration;

/**
 * <B>系统名称：</B>spring-cloud-alibaba 学习<BR>
 * <B>模块名称：</B>自定义ribbon配置<BR>
 * <B>中文类名：</B>自定义ribbon配置<BR>
 * <B>概要说明：</B>通过Java代码实现ribbon配置<BR>
 *
 * @author wsr
 * @since Created in 2019/8/29 11:26
 */
//@Configuration
//@RibbonClient(name = "user-center", configuration = RibbonConfiguration.class)
@RibbonClients(defaultConfiguration = RibbonConfiguration.class)
public class UserCenterRibbonConfiguration {
}
