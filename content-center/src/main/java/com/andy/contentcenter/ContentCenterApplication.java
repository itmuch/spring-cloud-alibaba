package com.andy.contentcenter;

import com.andy.contentcenter.configuration.GlobalFeignClientConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * <B>系统名称：</B>spring-cloud-alibaba-study<BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author wsr
 * @since Created in 2019/8/9 16:23
 */
@MapperScan("com.andy.contentcenter.mapper")
@SpringBootApplication
/** 设置Feign的全局配置*/
//@EnableFeignClients(defaultConfiguration = GlobalFeignClientConfiguration.class)
@EnableFeignClients
public class ContentCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContentCenterApplication.class,args);
    }

    // 在spring容器中，创建一个对象，类型RestTemplate；名称/ID是：restTemplate
    // <bean id="restTemplate" class="xxx.RestTemplate"/>
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        RestTemplate template = new RestTemplate();

        return template;
    }
}
