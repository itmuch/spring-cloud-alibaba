package com.andy.contentcenter.service;

import com.andy.contentcenter.domain.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
public interface NoticeService extends IService<Notice> {

}
