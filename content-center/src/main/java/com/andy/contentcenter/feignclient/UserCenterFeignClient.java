package com.andy.contentcenter.feignclient;

import com.andy.contentcenter.configuration.GlobalFeignClientConfiguration;
import com.andy.contentcenter.domain.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * <B>系统名称：</B>spring-cloud-alibaba 学习<BR>
 * <B>模块名称：</B>FeignClient<BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author wsr
 * @since Created in 2019/8/29 18:11
 */
//代码方式实现feign日志输出
// @FeignClient(name = "user-center",configuration = GlobalFeignClientConfiguration.class)
@FeignClient(name = "user-center",configuration = GlobalFeignClientConfiguration.class)

public interface UserCenterFeignClient {
    /**
     * <B>方法名称：</B>feign调用用户中心方法<BR>
     * <B>概要说明：</B>feign调用用户中心方法,实际生成路径：http://user-center/user/{id}<BR>
     * @author wsr
     * @since Created in 2019年8月29日18:15:15
     * @param id
     * @return
     */

    @GetMapping("/user/{id}")
    User findById(@PathVariable Integer id);
}
