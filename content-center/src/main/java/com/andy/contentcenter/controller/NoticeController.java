package com.andy.contentcenter.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
@RestController
@RequestMapping("/notice")
public class NoticeController {

}
