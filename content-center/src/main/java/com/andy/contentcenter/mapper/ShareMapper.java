package com.andy.contentcenter.mapper;

import com.andy.contentcenter.domain.entity.Share;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分享表 Mapper 接口
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
public interface ShareMapper extends BaseMapper<Share> {

}
