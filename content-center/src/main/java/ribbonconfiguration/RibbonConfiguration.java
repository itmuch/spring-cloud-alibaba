package ribbonconfiguration;

import com.andy.contentcenter.configuration.NacosSameClusterWeightedRule;
import com.andy.contentcenter.configuration.NacosWeightedRule;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <B>系统名称：</B>spring-cloud-alibaba 学习<BR>
 * <B>模块名称：</B>自定义ribbon配置<BR>
 * <B>中文类名：</B>自定义ribbon配置<BR>
 * <B>概要说明：</B>通过Java代码实现ribbon配置<BR>
 *
 * @author wsr
 * @since Created in 2019/8/29 11:26
 */
@Configuration
public class RibbonConfiguration {
    @Bean
    public IRule ribbonRule(){
        //return new RandomRule();
        //返回自定义Nacos权重配置
        //return new NacosWeightedRule();
        //返回自定义集群优先配置
        return new NacosSameClusterWeightedRule();
    }
}
